import './App.css';
import React from "react";
import {
    Route,
    Routes,
    BrowserRouter
} from "react-router-dom";
import About from './pages/About';
import Timeline from './pages/Timeline';
import Roll from './pages/Roll';
import Header from "./components/Header";
import {Container} from "@mui/material";
import Footer from "./components/Footer/Footer";

function App() {
    return (
        <BrowserRouter>
            <Container className="App">
                <Header className="Header"/>
                <Routes>
                    <Route path="/about" element={<About/>}/>
                    <Route path="/roll/:id" element={<Roll/>}/>
                    <Route path="/" element={<Timeline/>}/>
                </Routes>
                <Footer/>
            </Container>
        </BrowserRouter>
    );
}

export default App;
