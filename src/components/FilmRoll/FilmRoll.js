import styles from './FilmRoll.module.css';

const FilmRoll = () => {
    return (
        <>
            <div className={styles.film}>
                <ul>
                    <li>
                        <a href="http://instagram.com/p/VxWSkNsIoL/"
                           style={{backgroundImage: "url('https://i.imgur.com/Rv6w5D9.jpeg')"}}/>
                    </li>
                    <li>
                        <a href="http://instagram.com/p/SeVewsMIpT/"
                           style={{backgroundImage: "url('https://i.imgur.com/oNJO97c.jpg')"}}/>
                    </li>
                    <li>
                        <a href="http://instagram.com/p/Xd7ibYMIhC/"
                           style={{backgroundImage: "url('https://i.imgur.com/ykouQv2.jpg')"}}/>
                    </li>
                </ul>
            </div>
            <div className={styles.can}>
                <div className={styles.topCap}/>
                <div className={styles.text}>
                    <p className={styles.gold}>GOLD</p>
                    <p className={styles.num}>400</p>
                    <p className={styles.mm}>35mm film for color prints</p>
                </div>
                <div className={styles.bottomCap}/>
            </div>
        </>
    )
}
export default FilmRoll;