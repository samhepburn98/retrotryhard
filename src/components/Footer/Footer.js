import {Toolbar, Typography} from "@mui/material";
import styles from "../Footer/Footer.module.css";
import React from "react";

const Footer = () => {
    return (
        <div className={styles.headerContainer}>
            <Toolbar sx={{borderTop: 1, borderColor: 'divider'}} variant={"dense"}>
                <Typography
                    component="h2"
                    variant="h5"
                    color="inherit"
                    // align="center"
                    noWrap
                    sx={{flex: 1}}
                >
                    <p>© <span className={styles.copyright}>2021 Sam Hepburn</span></p>
                </Typography>
            </Toolbar>
        </div>
    )
}

export default Footer;