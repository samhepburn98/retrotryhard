import React from 'react';
import {Toolbar, Typography} from "@mui/material";
import InstagramIcon from '@mui/icons-material/Instagram';
import styles from "./Header.module.css";
import {useNavigate} from "react-router-dom";

const sections = [
    {title: 'Timeline', url: '/'},
    {title: 'About', url: '/about'},
    {title: 'Roll', url: '/roll'},
];

const Header = () => {
    const navigate = useNavigate();
    const instagramRedirect = () => {
        window.open("https://instagram.com/retrotryhard");
    }

    const homeRedirect = () => {
        navigate('/')
    }

    return (
        <div className={styles.headerContainer}>
            <Toolbar sx={{borderBottom: 1, borderColor: 'divider'}} variant={"dense"}>
                <Typography
                    component="h2"
                    variant="h5"
                    color="inherit"
                    align="center"
                    noWrap
                    sx={{flex: 1}}
                >
                    <p><span className={styles.title} onClick={homeRedirect}>retrotryhard</span></p>
                </Typography>
                <InstagramIcon className={styles.instagram} onClick={instagramRedirect}/>
            </Toolbar>
        </div>
    )
}

export default Header;