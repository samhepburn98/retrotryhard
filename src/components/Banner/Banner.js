import {ParallaxBanner} from "react-scroll-parallax";
import styles from "./Banner.module.css";
import {useNavigate} from "react-router-dom";

const Banner = ({id, img, subtitle, title}) => {
    const navigate = useNavigate();
    const handleClick = () => {
        navigate(`/roll/${id}`)
    }

    return (
        <div className={styles.bannerContainer} onClick={handleClick}>
            <ParallaxBanner
                layers={[
                    {
                        image: img,
                        amount: 0.4,
                    }
                ]}
            >
                <div className={styles.content}>
                    <h1>{title}</h1>
                    <p>{subtitle}</p>
                </div>
            </ParallaxBanner>
        </div>
    )
}

export default Banner;