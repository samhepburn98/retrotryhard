import {useParams} from "react-router-dom";
import Gallery from "react-photo-gallery";
import rollsIndex from "../../resources/rolls-index.json";
import Carousel, {Modal, ModalGateway} from "react-images";
import {useCallback, useState} from "react";
import styles from './Roll.module.css';

const findImages = (id) => {
    const rollData = rollsIndex.filter((item) => item.id === parseInt(id));
    let images = [];
    for (const img of rollData[0].images) {
        images.push({src: require(`../../resources/${img}`).default, width: 6, height: 4})
    }
    return images
}

const Roll = () => {
    const {id} = useParams();
    const images = findImages(id);

    const [currentImage, setCurrentImage] = useState(0);
    const [viewerIsOpen, setViewerIsOpen] = useState(false);

    const openLightbox = useCallback((event, {photo, index}) => {
        setCurrentImage(index);
        setViewerIsOpen(true);
    }, []);

    const closeLightbox = () => {
        setCurrentImage(0);
        setViewerIsOpen(false);
    };

    return (
        <div>
            <Gallery photos={images} onClick={openLightbox}/>
            <ModalGateway>
                {viewerIsOpen ? (
                    <Modal onClose={closeLightbox}>
                        <Carousel
                            currentIndex={currentImage}
                            views={images.map(x => ({
                                ...x,
                                srcset: x.srcSet,
                                caption: x.title
                            }))}
                        />
                    </Modal>
                ) : null}
            </ModalGateway>
        </div>
    )
}

export default Roll;