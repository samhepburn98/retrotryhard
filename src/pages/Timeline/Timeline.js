import Banner from "../../components/Banner";
import coverData from "../../resources/cover-data.json";

const Timeline = () => {
    return (
        <div>
            {coverData.map(cover => (
                    <Banner
                        id={cover.id}
                        img={require(`../../resources/${cover.path}`).default}
                        subtitle={cover.subtitle}
                        title={cover.title}
                    />
                )
            )}
        </div>
    )
}

export default Timeline;